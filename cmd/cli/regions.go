package main

import (
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/gosuri/uilive"
)

func getRegions(s *session.Session) []string {

	var out []string

	writer := uilive.New()
	// start listening for updates and render
	writer.Start()
	defer writer.Stop()

	fmt.Fprintf(writer, "Looking for the available regions...\n")

	out, err := exp.GetRegions(s)

	if err != nil {
		fmt.Printf("could not get the regions: %v\n", err)
		os.Exit(1)
	}

	fmt.Fprintf(writer, "Looking for the available regions...Done! Found %d regions.\n", len(out))

	return out
}
