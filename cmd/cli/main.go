package main

import (
	"fmt"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/gosuri/uilive"
	"github.com/gosuri/uiprogress"
	"github.com/spf13/viper"
	explorer "gitlab.com/ongresinc/labs/aws-explorer"
	"gitlab.com/ongresinc/labs/aws-explorer/tables"
)

var (
	mutex sync.Mutex
	wg    sync.WaitGroup
	exp   explorer.Config

	allAccounts      explorer.Accounts
	allInstances     []explorer.Instance
	allVolumes       []explorer.DiskVolume
	allSnapshots     []explorer.DiskSnapshot
	allIPs           []explorer.ElasticIP
	allNATGateways   []explorer.NATGateway
	allEksClusters   []explorer.EKSCluster
	allLoadBalancers []explorer.Loadbalancer
	allRDSInstances  []explorer.RDSInstance
	allRDSSnapshots  []explorer.RDSSnapshot
)

func init() {
	exp = explorer.New(viper.GetString("access-key"), viper.GetString("access-secret"), viper.GetString("region"))
}

func main() {

	s, err := exp.NewSession()

	if err != nil {
		fmt.Println("could not create an aws session: ", err)
		os.Exit(2)
	}

	processAccounts(s)
	allRegions := getRegions(s)
	uiprogress.RefreshInterval = 750 * time.Millisecond
	uiprogress.Start()

	wg.Add(len(allRegions))

	fmt.Println()
	fmt.Println("Checking services in all regions:")
	for _, region := range allRegions {
		session, err := exp.NewSessionReg(region)

		if err != nil {
			fmt.Printf("could not open session for region '%s': %v\n", region, err)
			continue
		}
		go processRegion(region, session, &wg)
	}

	wg.Wait()
	uiprogress.Stop()
	fmt.Println("Done!")
	fmt.Println()

	printer := tables.New(os.Stdout)

	printer.PrintAccounts(allAccounts)
	printer.PrintInstances(allInstances)
	printer.PrintVolumes(allVolumes)
	printer.PrintSnapshots(allSnapshots)
	printer.PrintEIPs(allIPs)
	printer.PrintNATGateways(allNATGateways)
	printer.PrintEKSClusters(allEksClusters)
	printer.PrintLoadbalancers(allLoadBalancers)
	printer.PrintRDSInstances(allRDSInstances)
	printer.PrintRDSSnapshot(allRDSSnapshots)

	fmt.Println()
}

func handleBarError(b *uiprogress.Bar, err error) {
	b.AppendFunc(func(b *uiprogress.Bar) string {
		return fmt.Sprintf("FAILED: %s", strings.Split(err.Error(), "\n"))
	})
}

func processAccounts(s *session.Session) {

	writer := uilive.New()
	writer.Start()
	defer writer.Stop()

	fmt.Fprintf(writer, "Looking for the org accounts...\n")

	var err error
	allAccounts, err = exp.GetAccounts(s)

	if err != nil {
		fmt.Printf("could not get the org accounts: %v\n", err)
		os.Exit(1)
	}

	fmt.Fprintf(writer, "Looking for the org accounts... Done! Found %d accounts.\n", len(allAccounts))
}

func processRegion(region string, sess *session.Session, wg *sync.WaitGroup) {

	defer wg.Done()

	bar := uiprogress.AddBar(1).AppendElapsed().AppendCompleted()
	bar.PrependFunc(func(b *uiprogress.Bar) string {
		return fmt.Sprintf("%15s", region)
	})

	report, err := exp.CompileReport(region)

	if err != nil {
		handleBarError(bar, err)
		return
	}

	mutex.Lock()
	allInstances = append(allInstances, report.Instances...)
	allVolumes = append(allVolumes, report.Volumes...)
	allSnapshots = append(allSnapshots, report.Snapshots...)
	allIPs = append(allIPs, report.ElaticIPs...)
	allNATGateways = append(allNATGateways, report.NATGateways...)
	allEksClusters = append(allEksClusters, report.EksClusters...)
	allLoadBalancers = append(allLoadBalancers, report.Loadbalancers...)
	allRDSInstances = append(allRDSInstances, report.RDSInstances...)
	allRDSSnapshots = append(allRDSSnapshots, report.RDSSnapshots...)
	mutex.Unlock()

	bar.Incr()
}
