package main

import (
	"fmt"
	"os"
	"strings"

	flag "github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const appName = "aws-explorer"

var (
	// Version is the current tag
	Version = "development"

	// Commit is the current last commit
	Commit = "latest"
)

func init() {

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "%s lists all objects created in AWS on all available regions.\n\n", appName)
		fmt.Fprintf(os.Stderr, "Usage:\n")
		fmt.Fprintf(os.Stderr, "  %s [OPTIONS]...\n\n", appName)
		fmt.Fprintf(os.Stderr, "General Options:\n")

		flag.PrintDefaults()
		fmt.Fprintf(os.Stderr, "\nReport BUGs on https://gitlab.com/ongresinc/labs/aws-explorer.\n\n")
	}

	flag.String("access-key", "", "AWS access key")
	flag.String("access-secret", "", "AWS access secret")
	flag.String("region", "us-east-1", "AWS region")

	flag.Parse()

	viper.BindPFlags(flag.CommandLine)
	viper.SetEnvPrefix("aws_explorer")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	viper.AutomaticEnv()

	fmt.Printf("Starting aws-explorer %s (%s)\n", Version, Commit)
}
