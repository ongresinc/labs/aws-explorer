package main

import (
	"bytes"
	"fmt"
	"log"
	"sync"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/spf13/viper"
	explorer "gitlab.com/ongresinc/labs/aws-explorer"
	"gitlab.com/ongresinc/labs/aws-explorer/tables"
)

func handleRequest() (string, error) {

	sess, err := exp.NewSession()

	if err != nil {
		return "", fmt.Errorf("could not create an aws session: %w", err)
	}

	regions, err := exp.GetRegions(sess)

	if err != nil {
		return "", fmt.Errorf("could not list regions: %w", err)
	}

	var report bytes.Buffer
	printer := tables.New(&report)

	wg.Add(len(regions))

	for i := 0; i < len(regions); i++ {

		go func(region string, wg *sync.WaitGroup) {

			defer wg.Done()

			log.Printf("Compiling report for the '%s' region...", region)
			report, err := exp.CompileReport(region)
			if err != nil {
				log.Printf("Could not process report: %s", err.Error())
				return
			}

			mutex.Lock()
			allReports.Instances = append(allReports.Instances, report.Instances...)
			allReports.Volumes = append(allReports.Volumes, report.Volumes...)
			allReports.Snapshots = append(allReports.Snapshots, report.Snapshots...)
			allReports.ElaticIPs = append(allReports.ElaticIPs, report.ElaticIPs...)
			allReports.NATGateways = append(allReports.NATGateways, report.NATGateways...)
			allReports.EksClusters = append(allReports.EksClusters, report.EksClusters...)
			allReports.Loadbalancers = append(allReports.Loadbalancers, report.Loadbalancers...)
			allReports.RDSInstances = append(allReports.RDSInstances, report.RDSInstances...)
			allReports.RDSSnapshots = append(allReports.RDSSnapshots, report.RDSSnapshots...)
			mutex.Unlock()
		}(regions[i], &wg)
	}

	wg.Wait()

	printer.PrintInstances(allReports.Instances)
	printer.PrintVolumes(allReports.Volumes)
	printer.PrintSnapshots(allReports.Snapshots)
	printer.PrintEIPs(allReports.ElaticIPs)
	printer.PrintNATGateways(allReports.NATGateways)
	printer.PrintEKSClusters(allReports.EksClusters)
	printer.PrintLoadbalancers(allReports.Loadbalancers)
	printer.PrintRDSInstances(allReports.RDSInstances)
	printer.PrintRDSSnapshot(allReports.RDSSnapshots)

	out, err := notify(report.String(), sess)

	if err != nil {
		return "", fmt.Errorf("could not send the notification: %w", err)
	}

	log.Println("SNS Topic sent", out)

	return "", nil
}

var (
	allReports explorer.Report

	mutex sync.Mutex
	wg    sync.WaitGroup
)

func main() {
	lambda.Start(handleRequest)
}

func notify(message string, sess *session.Session) (*sns.PublishOutput, error) {
	client := sns.New(sess)
	input := &sns.PublishInput{
		Message:  aws.String(message),
		TopicArn: aws.String(viper.GetString("topic")),
	}

	result, err := client.Publish(input)
	if err != nil {
		return nil, fmt.Errorf("Publish error: %w", err)
	}

	return result, nil
}
