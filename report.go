package explorer

import "fmt"

// Report contains all related metrics
type Report struct {
	Instances     []Instance
	Volumes       []DiskVolume
	Snapshots     []DiskSnapshot
	ElaticIPs     []ElasticIP
	NATGateways   []NATGateway
	EksClusters   []EKSCluster
	Loadbalancers []Loadbalancer
	RDSInstances  []RDSInstance
	RDSSnapshots  []RDSSnapshot
}

func compileReport(c Config, region string) (Report, error) {
	var out Report

	s, err := c.NewSessionReg(region)

	if err != nil {
		return out, fmt.Errorf("[CompileReport] could not create session: %v", err)
	}

	for _, collector := range actions {

		data, err := collector(region, s)

		if err != nil {
			return out, fmt.Errorf("[CompileReport] could not collect data: %v", err)
		}

		populateData(&out, data)
	}

	return out, nil
}

// CompileReport collect all metrics into a single object
func (c *Config) CompileReport(region string) (Report, error) {
	return compileReport(*c, region)
}
