package explorer

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
)

// Regions is a slice that contains all available regions
type Regions []string

func getRegions(c Config, s *session.Session) (Regions, error) {
	var out Regions

	svc := ec2.New(s)
	result, err := svc.DescribeRegions(nil)

	if err != nil {
		return out, fmt.Errorf("[GetRegions] could not describe regions: %v", err)
	}

	for _, r := range result.Regions {
		out = append(out, aws.StringValue(r.RegionName))
	}

	return out, nil
}

// GetRegions lists all available regions
func (c *Config) GetRegions(s *session.Session) (Regions, error) {
	return getRegions(*c, s)
}
