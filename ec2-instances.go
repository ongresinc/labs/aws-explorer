package explorer

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
)

// Instance is a EC2 instance
type Instance struct {
	Region string
	ID     string
	Tags   map[string]string
	Type   string
	State  string
}

func getInstances(region string, s *session.Session) ([]interface{}, error) {
	var out []interface{}

	svc := ec2.New(s)
	result, err := svc.DescribeInstances(nil)

	if err != nil {
		return nil, fmt.Errorf("could not get instances: %v", err)
	}

	for _, reserv := range result.Reservations {
		for _, inst := range reserv.Instances {
			out = append(out, &Instance{
				Region: region,
				ID:     *inst.InstanceId,
				Type:   *inst.InstanceType,
				Tags:   parseTags(inst.Tags),
				State:  *inst.State.Name,
			})
		}
	}

	return out, nil
}

func parseTags(tags []*ec2.Tag) map[string]string {

	var out = make(map[string]string)

	for _, tag := range tags {
		out[*tag.Key] = *tag.Value
	}

	return out
}
