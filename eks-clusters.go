package explorer

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/eks"
)

// EKSCluster is a EKS cluster in AWS
type EKSCluster struct {
	Region  string
	Arn     string
	Name    string
	Status  string
	Version string
	Tags    map[string]string
}

func getEKSClusters(region string, s *session.Session) ([]interface{}, error) {
	var out []interface{}

	svc := eks.New(s)
	allClusters, err := svc.ListClusters(nil)

	if err != nil {
		return nil, fmt.Errorf("could not get the eks clusters: %v", err)
	}

	for _, cluster := range allClusters.Clusters {

		filter := &eks.DescribeClusterInput{
			Name: cluster,
		}

		eks, err := svc.DescribeCluster(filter)

		if err != nil {
			return nil, fmt.Errorf("could not describe the EKS cluster: %v", err)
		}

		out = append(out, &EKSCluster{
			Region:  region,
			Arn:     aws.StringValue(eks.Cluster.Arn),
			Name:    aws.StringValue(eks.Cluster.Name),
			Status:  aws.StringValue(eks.Cluster.Status),
			Version: aws.StringValue(eks.Cluster.Version),
			Tags:    parseEksTags(eks.Cluster.Tags),
		})
	}

	return out, nil
}

func parseEksTags(in map[string]*string) map[string]string {
	var out = make(map[string]string)

	for k, v := range in {
		out[k] = aws.StringValue(v)
	}

	return out
}
