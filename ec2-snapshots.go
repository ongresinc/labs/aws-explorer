package explorer

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
)

// DiskSnapshot is a Snapshot in AWS
type DiskSnapshot struct {
	Region     string
	ID         string
	State      string
	OwnerID    string
	OwnerAlias string
	Tags       map[string]string
	VolumeID   string
	VolumeSize int64
}

var allAccounts Accounts

func getSnapshots(region string, s *session.Session) ([]interface{}, error) {
	var out []interface{}
	var err error

	if len(allAccounts) == 0 {
		allAccounts, err = getAccounts(s)

		if err != nil {
			return nil, fmt.Errorf("could not get the accounts: %v", err)
		}
	}

	svc := ec2.New(s)
	input := &ec2.DescribeSnapshotsInput{
		OwnerIds: allAccounts.ToPointerSlice(),
	}
	result, err := svc.DescribeSnapshots(input)

	if err != nil {
		return nil, fmt.Errorf("could not get the snapshots: %v", err)
	}

	for _, snap := range result.Snapshots {

		out = append(out, &DiskSnapshot{
			Region:     region,
			ID:         *snap.SnapshotId,
			State:      *snap.State,
			Tags:       parseTags(snap.Tags),
			VolumeID:   *snap.VolumeId,
			VolumeSize: *snap.VolumeSize * GiB,
			OwnerID:    *snap.OwnerId,
			OwnerAlias: aws.StringValue(snap.OwnerAlias),
		})
	}

	return out, nil
}
