package explorer

// Config contains the connection info of AWS
type Config struct {
	key    string
	secret string
	Region string
}

func newConf(key, secret, region string) Config {
	var out Config

	out = Config{
		key:    key,
		secret: secret,
	}

	if region != "" {
		out.Region = region
	}

	return out
}

// New creates a basic Config
func New(key, secret, region string) Config {
	return newConf(key, secret, region)
}
