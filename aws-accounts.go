package explorer

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/organizations"
)

// Account is a AWS acount bound to an organization
type Account struct {
	ID     string
	ARN    string
	Status string
	Name   string
}

// Accounts is a collection of Account
type Accounts []Account

// ToPointerSlice creates a slice of string pointers that contains all account IDs
func (list *Accounts) ToPointerSlice() []*string {
	var out []*string

	for _, a := range *list {
		out = append(out, &a.ID)
	}

	return out
}

func getAccounts(s *session.Session) (Accounts, error) {
	var out Accounts

	svc := organizations.New(s)
	result, err := svc.ListAccounts(nil)

	if err != nil {
		return nil, fmt.Errorf("could not get the accounts: %v", err)
	}

	for _, a := range result.Accounts {

		out = append(out, Account{
			ID:     aws.StringValue(a.Id),
			ARN:    aws.StringValue(a.Arn),
			Status: aws.StringValue(a.Status),
			Name:   aws.StringValue(a.Name),
		})
	}

	allAccounts = out

	return out, nil
}

// GetAccounts lists all AWS accounts from the organization
func (c *Config) GetAccounts(s *session.Session) (Accounts, error) {
	return getAccounts(s)
}
