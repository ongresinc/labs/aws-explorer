# AWS Explorer

This tool lists objects on all AWS regions making them easy to track.

## TODO

### Features

- [ ] support multiple accounts
- [ ] filter objects by tag
- [ ] add option to dele objects with confirmation

### Collectors

- [x] EC2 instances
- [x] ELBs (of any kind, classic ELB, ALB, NLB)
- [x] NAT Gateways
- [x] Clusters EKS
- [x] EBS volumes
- [x] EBS Snapshots
- [x] Elastic IPs
- [x] Organization Accounts
- [x] RDS instances and snapshots
