package explorer

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/elb"
	"github.com/aws/aws-sdk-go/service/elbv2"
)

// Loadbalancer is a EKS cluster in AWS
type Loadbalancer struct {
	Region            string
	Name              string
	VpcID             string
	Type              string
	SecurityGroups    []string
	AvailabilityZones []string
}

func getLoadbalancers(region string, s *session.Session) ([]interface{}, error) {
	var out []interface{}

	svcV2 := elbv2.New(s)
	result, err := svcV2.DescribeLoadBalancers(nil)

	if err != nil {
		return nil, fmt.Errorf("could not get the load balancers: %v", err)
	}

	for _, lb := range result.LoadBalancers {
		out = append(out, &Loadbalancer{
			Region:            region,
			Name:              aws.StringValue(lb.LoadBalancerName),
			VpcID:             aws.StringValue(lb.VpcId),
			Type:              aws.StringValue(lb.Type),
			SecurityGroups:    aws.StringValueSlice(lb.SecurityGroups),
			AvailabilityZones: processAZ(lb.AvailabilityZones),
		})
	}

	svcV1 := elb.New(s)
	result2, err := svcV1.DescribeLoadBalancers(nil)

	if err != nil {
		return nil, fmt.Errorf("could not get the load balancers: %v", err)
	}

	for _, lb := range result2.LoadBalancerDescriptions {
		out = append(out, &Loadbalancer{
			Region:            region,
			Name:              aws.StringValue(lb.LoadBalancerName),
			VpcID:             aws.StringValue(lb.VPCId),
			Type:              "classic",
			SecurityGroups:    aws.StringValueSlice(lb.SecurityGroups),
			AvailabilityZones: aws.StringValueSlice(lb.AvailabilityZones),
		})
	}

	return out, nil
}

func processAZ(azList []*elbv2.AvailabilityZone) []string {
	var out []string

	for _, az := range azList {
		out = append(out, aws.StringValue(az.ZoneName))
	}

	return out
}
