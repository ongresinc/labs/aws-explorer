package explorer

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
)

// DiskVolume is a EBS volume in AWS
type DiskVolume struct {
	Region           string
	ID               string
	Type             string
	AvailabilityZone string
	State            string
	Tags             map[string]string
	Size             int64
	Snapshot         string
	Attachments      []VolAttachment
}

// VolAttachment describes a EBS attachment on EC2
type VolAttachment struct {
	InstanceID string
	State      string
}

func getVolumes(region string, s *session.Session) ([]interface{}, error) {
	var out []interface{}

	svc := ec2.New(s)
	result, err := svc.DescribeVolumes(nil)

	if err != nil {
		return nil, fmt.Errorf("could not get volumes: %v", err)
	}

	for _, vol := range result.Volumes {

		var attachList []VolAttachment

		for _, a := range vol.Attachments {
			attachList = append(attachList, VolAttachment{
				InstanceID: *a.InstanceId,
				State:      *a.State,
			})
		}

		out = append(out, &DiskVolume{
			Region:           region,
			ID:               *vol.VolumeId,
			Type:             *vol.VolumeType,
			State:            *vol.State,
			Tags:             parseTags(vol.Tags),
			Size:             *vol.Size * GiB,
			Attachments:      attachList,
			Snapshot:         *vol.SnapshotId,
			AvailabilityZone: *vol.AvailabilityZone,
		})
	}

	return out, nil
}
