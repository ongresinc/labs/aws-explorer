package explorer

const (
	// B is a Byte
	B = 1

	// KiB is a KiloByte
	KiB = B * 1024

	// MiB is a Megabyte
	MiB = KiB * 1024

	// GiB is a Gigabyte
	GiB = MiB * 1024

	// TiB is a TeraByte
	TiB = GiB * 1024
)
