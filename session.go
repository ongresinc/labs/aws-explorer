package explorer

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
)

func buildSession(c *Config) (*session.Session, error) {
	var out *session.Session
	var err error

	conf := aws.NewConfig()

	if c.key != "" && c.secret != "" {
		conf.Credentials = credentials.NewStaticCredentials(c.key, c.secret, "")
	}

	if c.Region != "" {
		conf.Region = aws.String(c.Region)
	}

	out, err = session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
		Config:            *conf,
	})

	return out, err
}

// NewSession creates a session in AWS
func (c *Config) NewSession() (*session.Session, error) {
	return buildSession(c)
}

// NewSessionReg creates a session in AWS on a specific region
func (c *Config) NewSessionReg(reg string) (*session.Session, error) {
	c.Region = reg

	return buildSession(c)
}
