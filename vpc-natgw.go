package explorer

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
)

// NATGateway is a NatGateway inside a vpc in AWS
type NATGateway struct {
	Region string
	ID     string
	State  string
	VpcID  string
	Tags   map[string]string
}

func getNatGateways(region string, s *session.Session) ([]interface{}, error) {
	var out []interface{}

	svc := ec2.New(s)
	result, err := svc.DescribeNatGateways(nil)

	if err != nil {
		return nil, fmt.Errorf("could not get instances: %v", err)
	}

	for _, nGW := range result.NatGateways {
		out = append(out, &NATGateway{
			Region: region,
			ID:     aws.StringValue(nGW.NatGatewayId),
			State:  aws.StringValue(nGW.State),
			VpcID:  aws.StringValue(nGW.VpcId),
			Tags:   parseTags(nGW.Tags),
		})
	}

	return out, nil
}
