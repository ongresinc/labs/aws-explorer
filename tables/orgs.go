package tables

import (
	"github.com/alexeyco/simpletable"
	explorer "gitlab.com/ongresinc/labs/aws-explorer"
)

func accToCell(a explorer.Account) []*simpletable.Cell {
	return []*simpletable.Cell{
		{Align: simpletable.AlignLeft, Text: a.ID},
		{Align: simpletable.AlignLeft, Text: a.Name},
		{Align: simpletable.AlignCenter, Text: a.Status},
		{Align: simpletable.AlignLeft, Text: a.ARN},
	}
}

// PrintAccounts displays accounts information
func (c *Config) PrintAccounts(list explorer.Accounts) {

	table := simpletable.New()

	table.Header = &simpletable.Header{
		Cells: []*simpletable.Cell{
			{Align: simpletable.AlignCenter, Text: "ID"},
			{Align: simpletable.AlignCenter, Text: "Name"},
			{Align: simpletable.AlignCenter, Text: "Status"},
			{Align: simpletable.AlignCenter, Text: "ARN"},
		},
	}

	for _, a := range list {
		table.Body.Cells = append(table.Body.Cells, accToCell(a))
	}

	printTable(c.Writer, "All Organization accounts", table)
}
