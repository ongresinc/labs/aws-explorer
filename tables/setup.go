package tables

import "io"

// Config stores the table configuration
type Config struct {
	Writer io.Writer
}

// New creates a new table configuration
func New(w io.Writer) *Config {
	return &Config{Writer: w}
}
