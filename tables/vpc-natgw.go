package tables

import (
	"github.com/alexeyco/simpletable"
	explorer "gitlab.com/ongresinc/labs/aws-explorer"
)

func gwToCell(nGW explorer.NATGateway) []*simpletable.Cell {
	return []*simpletable.Cell{
		{Align: simpletable.AlignCenter, Text: nGW.Region},
		{Align: simpletable.AlignCenter, Text: nGW.ID},
		{Align: simpletable.AlignCenter, Text: nGW.State},
		{Align: simpletable.AlignLeft, Text: processTags(nGW.Tags)},
	}
}

// PrintNATGateways display NAT Gateways information
func (c *Config) PrintNATGateways(natGateways []explorer.NATGateway) {

	table := simpletable.New()

	table.Header = &simpletable.Header{
		Cells: []*simpletable.Cell{
			{Align: simpletable.AlignCenter, Text: "Region"},
			{Align: simpletable.AlignCenter, Text: "ID"},
			{Align: simpletable.AlignCenter, Text: "State"},
			{Align: simpletable.AlignCenter, Text: "Tags"},
		},
	}

	for _, nGW := range natGateways {
		table.Body.Cells = append(table.Body.Cells, gwToCell(nGW))
	}

	printTable(c.Writer, "All VPC NAT Gateways", table)
}
