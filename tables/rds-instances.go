package tables

import (
	"fmt"

	"github.com/alexeyco/simpletable"
	"github.com/dustin/go-humanize"
	explorer "gitlab.com/ongresinc/labs/aws-explorer"
)

func rdsInstToCell(r explorer.RDSInstance) []*simpletable.Cell {
	return []*simpletable.Cell{
		{Align: simpletable.AlignCenter, Text: r.Region},
		{Align: simpletable.AlignCenter, Text: r.Name},
		{Align: simpletable.AlignCenter, Text: r.Class},
		{Align: simpletable.AlignCenter, Text: r.Status},
		{Align: simpletable.AlignCenter, Text: r.Engine},
		{Align: simpletable.AlignCenter, Text: r.Version},
		{Align: simpletable.AlignCenter, Text: humanize.IBytes(uint64(r.DiskSize))},
	}
}

// PrintRDSInstances display RDS instances
func (c *Config) PrintRDSInstances(instances []explorer.RDSInstance) {

	table := simpletable.New()

	table.Header = &simpletable.Header{
		Cells: []*simpletable.Cell{
			{Align: simpletable.AlignCenter, Text: "Region"},
			{Align: simpletable.AlignCenter, Text: "Name"},
			{Align: simpletable.AlignCenter, Text: "Class"},
			{Align: simpletable.AlignCenter, Text: "State"},
			{Align: simpletable.AlignCenter, Text: "Engine"},
			{Align: simpletable.AlignCenter, Text: "Version"},
			{Align: simpletable.AlignCenter, Text: "DiskSize"},
		},
	}

	var allocSize int64
	for _, i := range instances {
		table.Body.Cells = append(table.Body.Cells, rdsInstToCell(i))

		allocSize += i.DiskSize
	}

	printTable(c.Writer, "All RDS instances", table)

	if allocSize > 0 {
		fmt.Fprintf(c.Writer, "Total size: %s\n", humanize.IBytes(uint64(allocSize)))
	}
}
