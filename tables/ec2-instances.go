package tables

import (
	"github.com/alexeyco/simpletable"
	explorer "gitlab.com/ongresinc/labs/aws-explorer"
)

func instToCell(i explorer.Instance) []*simpletable.Cell {
	return []*simpletable.Cell{
		{Align: simpletable.AlignCenter, Text: i.Region},
		{Align: simpletable.AlignCenter, Text: i.ID},
		{Align: simpletable.AlignCenter, Text: getKey(i.Tags, "Name")},
		{Align: simpletable.AlignCenter, Text: i.Type},
		{Align: simpletable.AlignCenter, Text: i.State},
		{Align: simpletable.AlignLeft, Text: processTags(i.Tags)},
	}
}

// PrintInstances writes a table with AWS Instances
func (c *Config) PrintInstances(instances []explorer.Instance) {

	table := simpletable.New()

	table.Header = &simpletable.Header{
		Cells: []*simpletable.Cell{
			{Align: simpletable.AlignCenter, Text: "Region"},
			{Align: simpletable.AlignCenter, Text: "InstanceID"},
			{Align: simpletable.AlignCenter, Text: "Name"},
			{Align: simpletable.AlignCenter, Text: "Type"},
			{Align: simpletable.AlignCenter, Text: "State"},
			{Align: simpletable.AlignCenter, Text: "Tags"},
		},
	}

	for _, i := range instances {
		table.Body.Cells = append(table.Body.Cells, instToCell(i))
	}

	printTable(c.Writer, "All EC2 instances", table)
}
